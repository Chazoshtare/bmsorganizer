# BMSOrganizer

This will be a BMS Player-agnostic BMS organizer tool for merging duplicates, renaming etc.  
For now, it can only find and merge folders with duplicates.  
Available for Windows and Linux.

## WARNING - READ THIS!

This tool is highly experimental, and might mess up your files or BMS collection if you don't use it as intended.

## Usage

Just run the exe on Windows or the kexe on Linux.  
Use `Add root folder` button to add a root folder containing your BMS packs and `Add single folder` to add any
individual ones. `Clear` will clear your choices. BMSOrganizer assumes your BMS pack folder structure is flat - it only
scans song folders that are put exactly into selected paths. It will ignore any further nested folders.

### Clean selected packs

Add all folders/packs you want and click `Clean selected packs`. This will scan every song folder separately, and delete
all redundant BMS copies in single folders. Don't use this if you keep some spare copies for charting/editing etc.

### Scan for duplicates

Add all folders/packs you want and click `Scan for duplicates!` to perform a scan. Found duplicates with paths and sound
file types will be displayed on the bottom. *It will take a lot of time if your collection is huge.*  
You can either choose to merge all displayed duplicates into a single selected folder with `Merge to here`, or ignore
them. Chosen directory will be the one left, others will be merged to it and deleted.

tl;dr: Add root/single folder -> Scan -> Merge to here/Ignore

**Don't worry about mixed slashes `/ vs \ ` in any displayed paths, I'll deal with that later, but just in case
everything works fine.**

**Currently, merging directories with different sound formats (e.g. one of them `[OGG]` and other `[WAV]`) is _not
recommended_**

## Screenshots

![linux-build](screenshots/bmsorganizer-linux.png "Linux Build")

## Building

#### Windows

Until I split the project into two platforms or figure out a better way to solve some problems, you have to check
out `windows-build-temp` branch and only then run the build. So:

```shell
git checkout windows-build-temp
./gradlew build
```

#### Linux

Install whatever gtk3 dev package your distro uses. Then:

```shell
./gradlew build
```

## Contributing

Any PRs, suggestions, complaints and whatever are welcome. You can create issues, email me or contact me on
discord [Chazoshtare#3769].

## TODO

- Windows screenshots
- Split into two platforms or resolve mkdir+slashes issues differently (currently waiting for newer commonizer)
- Process packs and merging with coroutines (wait for https://github.com/Kotlin/kotlinx.coroutines/issues/462)
- Resistance to unexpected user input
- How to merge WAV vs OGG folders
