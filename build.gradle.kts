plugins {
    kotlin("multiplatform") version "1.6.10"
}

group = "com.sneknology"
version = "0.1.0"

repositories {
    mavenCentral()
    jcenter()
}

kotlin {
    val hostOs = System.getProperty("os.name")
    val isMingwX64 = hostOs.startsWith("Windows")
    val nativeTarget = when {
//        hostOs == "Mac OS X" -> macosX64("native")
        hostOs == "Linux" -> linuxX64("native")
        isMingwX64 -> mingwX64("native")
        else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
    }

    nativeTarget.apply {
        binaries.executable {
            entryPoint = "com.sneknology.bmsorganizer.main"

            if (isMingwX64) {
                windowsResources("bmsorganizer.rc")
                linkerOpts("-mwindows")
            }
        }
    }

    sourceSets {
        // common -> native (linux or windows)
        val commonMain by getting {
            dependencies {
                implementation("com.soywiz.korlibs.krypto:krypto:2.6.3")
            }
        }
        val commonTest by getting

        val nativeMain by getting {
            dependencies {
                implementation("com.github.msink:libui:0.1.8")
            }
        }
        val nativeTest by getting
    }
}

fun org.jetbrains.kotlin.gradle.plugin.mpp.Executable.windowsResources(rcFileName: String) {
    val taskName = linkTaskName.replaceFirst("link", "windres")
    val inFile = compilation.defaultSourceSet.resources.sourceDirectories.singleFile.resolve(rcFileName)
    val outFile = buildDir.resolve("processedResources/$taskName.res")

    val windresTask = tasks.create<Exec>(taskName) {
        val konanUserDir = System.getenv("KONAN_DATA_DIR") ?: "${System.getProperty("user.home")}/.konan"
        val konanLlvmDir = "$konanUserDir/dependencies/msys2-mingw-w64-x86_64-clang-llvm-lld-compiler_rt-8.0.1/bin"

        inputs.file(inFile)
        outputs.file(outFile)
        commandLine("$konanLlvmDir/windres", inFile, "-D_${buildType.name}", "-O", "coff", "-o", outFile)
        environment("PATH", "$konanLlvmDir;${System.getenv("PATH")}")

        dependsOn(compilation.compileKotlinTask)
    }

    linkTask.dependsOn(windresTask)
    linkerOpts(outFile.toString())
}
