package com.sneknology.bmsorganizer.model

data class DuplicatedDirectoryInfo(
    val directoryPath: Path,
    val oggFileNumber: Int,
    val wavFileNumber: Int
) {

    fun stringRepresentation() = "${getSoundTypesString()} ${directoryPath.value}"

    private fun getSoundTypesString(): String {
        val ogg = if (oggFileNumber > 0) "OGG" else ""
        val wav = if (wavFileNumber > 0) "WAV" else ""
        return "[" + "$ogg/$wav".removePrefix("/").removeSuffix("/") + "]"
    }
}
