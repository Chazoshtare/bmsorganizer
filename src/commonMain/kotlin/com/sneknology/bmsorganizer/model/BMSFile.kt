package com.sneknology.bmsorganizer.model

data class BMSFile(
    val filePath: Path,
    val hash: String,
    val songDirectory: Path,
    val oggInDirectory: Int,
    val wavInDirectory: Int
) {

    fun toDuplicatedDirectoryInfo() = DuplicatedDirectoryInfo(
        songDirectory, oggInDirectory, wavInDirectory
    )
}
