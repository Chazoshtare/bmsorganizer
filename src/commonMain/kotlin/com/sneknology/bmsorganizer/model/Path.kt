package com.sneknology.bmsorganizer.model

data class Path(val value: String) {

    val fileName = value.removeSuffix("/").substringAfterLast("/")
    val extension = fileName.substringAfterLast(".", "").let { if (it == "") null else it }

    fun append(path: String) = Path("$value/$path")

    fun hasBMSFileExtension() = BMS_EXTENSIONS.any { it.equals(extension, true) }
    fun hasWAVFileExtension() = extension.equals(WAV_EXTENSION, true)
    fun hasOGGFileExtension() = extension.equals(OGG_EXTENSION, true)
    fun isAPreview() = fileName.startsWith("preview") && (hasOGGFileExtension() || hasWAVFileExtension())

    override fun toString() = value

    companion object {
        private val BMS_EXTENSIONS = setOf("bms", "bme", "bml", "pms", "bmson")
        private const val WAV_EXTENSION = "wav"
        private const val OGG_EXTENSION = "ogg"
    }
}

expect fun Path.isADirectory(): Boolean

expect fun Path.exists(): Boolean

expect fun Path.getAllPathsInDirectory(filter: (Path) -> Boolean = { true }): Set<Path>

expect fun Path.forEachPathInDirectory(action: (Path) -> Unit)
