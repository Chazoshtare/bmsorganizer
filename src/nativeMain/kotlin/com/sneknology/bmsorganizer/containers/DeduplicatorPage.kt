package com.sneknology.bmsorganizer.containers

import com.sneknology.bmsorganizer.bms.DuplicatesOperations
import com.sneknology.bmsorganizer.bms.DuplicatesOperations.resolveDuplicate
import com.sneknology.bmsorganizer.files.DirectoryScanner
import com.sneknology.bmsorganizer.model.DuplicatedDirectoryInfo
import com.sneknology.bmsorganizer.model.Path
import com.sneknology.bmsorganizer.model.exists
import libui.ktx.*

fun TabPane.deduplicator() =
    page("Deduplicator") {
        vbox {
            lateinit var bmsPacksTableBox: VBox
            lateinit var bmsPacksTable: TableView
            lateinit var duplicateResolutionBox: VBox
            val bmsPacksPaths = mutableSetOf<Path>()
            val duplicateGroups = mutableListOf<List<DuplicatedDirectoryInfo>>()

            hbox {
                button("Add root folder") {
                    action {
                        OpenFolderDialog()?.let { chosenFolder ->
                            val allDirectoriesInPath = DirectoryScanner.getAllDirectoriesInPath(Path(chosenFolder))
                            bmsPacksPaths.addAll(allDirectoriesInPath)
                            bmsPacksTable = bmsPacksTableBox.refreshSelectedPacksTable(
                                bmsPacksPaths.toList(),
                                bmsPacksTable
                            )
                        }
                    }
                }
                button("Add single folder") {
                    action {
                        OpenFolderDialog()?.let {
                            val chosenFolder = Path(it)
                            bmsPacksPaths.add(chosenFolder)
                            bmsPacksTable = bmsPacksTableBox.refreshSelectedPacksTable(
                                bmsPacksPaths.toList(),
                                bmsPacksTable
                            )
                        }
                    }
                }
                button("Clear") {
                    action {
                        bmsPacksPaths.clear()
                        bmsPacksTable = bmsPacksTableBox.refreshSelectedPacksTable(
                            bmsPacksPaths.toList(),
                            bmsPacksTable
                        )
                    }
                }
            }
            hbox {
                button("Scan for duplicates!") {
                    action {
                        duplicateGroups.addAll(DuplicatesOperations.findDuplicates(bmsPacksPaths))
                    }
                }
                button("Clean selected packs") {
                    action {
                        val deletedFilesCount = DuplicatesOperations.cleanPacks(bmsPacksPaths)
                        MsgBox("Deleted $deletedFilesCount duplicated BMS files.")
                    }
                }
            }

            bmsPacksTableBox = group("Selected BMS packs") { stretchy = true }.vbox {
                bmsPacksTable = tableview(listOf<Nothing>()) { stretchy = true }
            }

            val duplicatesBox: VBox = group("Resolve duplicates").vbox

            var currentlyProcessed: List<DuplicatedDirectoryInfo>? = null
            onTimer(100) {
                if (duplicateGroups.isNotEmpty() && currentlyProcessed == null) {

                    currentlyProcessed = duplicateGroups.removeFirst()
                    if (currentlyProcessed!!.all { it.directoryPath.exists() }) {
                        duplicatesBox.apply {
                            duplicateResolutionBox = vbox {
                                currentlyProcessed!!.forEachIndexed { index, directoryInfo ->
                                    hbox {
                                        this.padded = true
                                        button("Merge to here") {
                                            action {
                                                resolveDuplicate(currentlyProcessed!!, index)
                                                duplicatesBox.delete(0)
                                                duplicateResolutionBox.dispose()
                                                currentlyProcessed = null
                                            }
                                        }
                                        label(directoryInfo.stringRepresentation())
                                    }
                                }
                                button("Ignore") {
                                    action {
                                        duplicatesBox.delete(0)
                                        duplicateResolutionBox.dispose()
                                        currentlyProcessed = null
                                    }
                                }
                            }
                        }
                    } else {
                        currentlyProcessed = null
                    }
                }
                true
            }
        }
    }

private fun VBox.refreshSelectedPacksTable(newPacks: List<Path>, currentTable: TableView): TableView {
    delete(0)
    currentTable.dispose()

    return tableview(newPacks) {
        stretchy = true
        column("Name") { label(Path::fileName) }
        column("Full Path") { label(Path::value) }
    }
}
