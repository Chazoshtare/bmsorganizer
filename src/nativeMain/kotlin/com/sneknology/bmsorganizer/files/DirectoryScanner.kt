package com.sneknology.bmsorganizer.files

import com.sneknology.bmsorganizer.files.FileReader.fileContentToByteArray
import com.sneknology.bmsorganizer.model.BMSFile
import com.sneknology.bmsorganizer.model.Path
import com.sneknology.bmsorganizer.model.forEachPathInDirectory
import com.sneknology.bmsorganizer.model.isADirectory
import com.soywiz.krypto.sha256

object DirectoryScanner {

    fun getAllDirectoriesInPath(path: Path): List<Path> {
        val bmsPacksPaths = mutableListOf<Path>()
        path.forEachPathInDirectory {
            if (it.isADirectory()) {
                bmsPacksPaths.add(it)
            }
        }
        return bmsPacksPaths
    }

    fun getAllBMSFilesInPaths(paths: Collection<Path>): List<BMSFile> =
        paths.map { Path(it.value.removeSuffix("/")) }
            .flatMap { packPath ->
                getAllDirectoriesInPath(packPath).flatMap { songPath ->
                    getAllBMSFilesInPath(songPath)
                }
            }

    private fun getAllBMSFilesInPath(songPath: Path): List<BMSFile> {
        val bmsFilesPaths = mutableListOf<Path>()
        var wavFilesNumber = 0
        var oggFilesNumber = 0

        songPath.forEachPathInDirectory {
            if (!it.isADirectory() && !it.isAPreview()) {
                when {
                    it.hasOGGFileExtension() -> oggFilesNumber++
                    it.hasWAVFileExtension() -> wavFilesNumber++
                    it.hasBMSFileExtension() -> bmsFilesPaths.add(it)
                }
            }
        }

        return bmsFilesPaths.map {
            BMSFile(
                it,
                fileContentToByteArray(it).sha256().hex,
                songPath,
                oggFilesNumber,
                wavFilesNumber
            )
        }
    }
}
