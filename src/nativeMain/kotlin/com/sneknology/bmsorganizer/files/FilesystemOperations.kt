package com.sneknology.bmsorganizer.files

import com.sneknology.bmsorganizer.model.Path
import com.sneknology.bmsorganizer.model.exists
import com.sneknology.bmsorganizer.model.forEachPathInDirectory
import com.sneknology.bmsorganizer.model.isADirectory
import kotlinx.cinterop.ByteVar
import kotlinx.cinterop.allocArray
import kotlinx.cinterop.memScoped
import platform.posix.*

object FilesystemOperations {

    fun moveDirectory(source: Path, destination: Path) {
        println("Copying [$source] to [$destination]")
        copyDirectory(source, destination)
        println("Deleting [$source]\n")
        delete(source)
    }

    /**
     * Deletes a file/directory with a given [path].
     */
    fun delete(path: Path) {
        if (path.isADirectory()) deleteDirectory(path) else remove(path.value)
    }

    private fun createDirectory(path: Path) {
        if (!path.exists()) {
            // TODO: make an interop function for Linux/Windows (currently waiting for newer commonizer to split this)
            mkdir(path.value, 16877) // 16877 is UInt for 755 mode
        }
    }

    private fun copyDirectory(source: Path, destination: Path) {
        require(source.exists()) { "Directory $source does not exist!" }
        createDirectory(destination)

        source.forEachPathInDirectory {
            val innerDestination = destination.append(it.fileName)
            if (it.isADirectory()) {
                copyDirectory(it, innerDestination)
            } else {
                copyFile(it, innerDestination)
            }
        }
    }

    /**
     * Deletes a directory and its contents. Has the same effect as `rm -rf [path]`.
     */
    private fun deleteDirectory(path: Path) {
        path.forEachPathInDirectory {
            if (it.isADirectory()) deleteDirectory(it) else remove(it.value)
        }
        rmdir(path.value)
    }

    private fun copyFile(source: Path, destination: Path) {
        val sourceFile = fopen(source.value, "rb")
        val destinationFile = fopen(destination.value, "wb")
        try {
            if (sourceFile != null && destinationFile != null) {
                memScoped {
                    val buffer = allocArray<ByteVar>(BUFSIZ)
                    do {
                        val size = fread(buffer, 1, BUFSIZ, sourceFile)
                        if (size > 0u) {
                            fwrite(buffer, 1, size, destinationFile)
                        }
                    } while (size > 0u)
                }
            }
        } finally {
            fclose(sourceFile)
            fclose(destinationFile)
        }
    }
}
