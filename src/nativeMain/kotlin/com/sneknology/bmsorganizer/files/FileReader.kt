package com.sneknology.bmsorganizer.files

import com.sneknology.bmsorganizer.model.Path
import kotlinx.cinterop.ByteVar
import kotlinx.cinterop.allocArray
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.toKString
import platform.posix.fclose
import platform.posix.fgets
import platform.posix.fopen

object FileReader {

    fun fileContentToByteArray(path: Path): ByteArray {
        val returnBuffer = StringBuilder()
        val file = fopen(path.value, "r") ?: throw IllegalArgumentException("File $path does not exist!")

        try {
            memScoped {
                val readBufferLength = 64 * 1024
                val buffer = allocArray<ByteVar>(readBufferLength)
                var line = fgets(buffer, readBufferLength, file)?.toKString()
                while (line != null) {
                    returnBuffer.append(line)
                    line = fgets(buffer, readBufferLength, file)?.toKString()
                }
            }
        } finally {
            fclose(file)
        }

        return returnBuffer.toString().encodeToByteArray()
    }
}
