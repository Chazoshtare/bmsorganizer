package com.sneknology.bmsorganizer.bms

import com.sneknology.bmsorganizer.files.DirectoryScanner
import com.sneknology.bmsorganizer.files.FileReader
import com.sneknology.bmsorganizer.files.FilesystemOperations
import com.sneknology.bmsorganizer.model.*
import com.soywiz.krypto.sha256

object DuplicatesOperations {

    /**
     * Searches for BMS duplicates in all [bmsPacks]. Only descends one directory deeper in each BMS pack to find song
     * directories.
     * Returns a list of duplicate groups, each representing a list of directories containing same BMS files.
     */
    fun findDuplicates(bmsPacks: Collection<Path>): List<List<DuplicatedDirectoryInfo>> {
        val bmsFiles = DirectoryScanner.getAllBMSFilesInPaths(bmsPacks)
        return bmsFiles.groupBy(BMSFile::hash, BMSFile::toDuplicatedDirectoryInfo)
            .map { it.value.distinct().sortedBy { directory -> directory.directoryPath.value } }
            .filter { it.size > 1 }
            .distinct()
    }

    fun resolveDuplicate(duplicateGroup: List<DuplicatedDirectoryInfo>, choiceIndex: Int) {
        val destination = duplicateGroup[choiceIndex]
        duplicateGroup.onEachIndexed { index, source ->
            if (index != choiceIndex) {
                FilesystemOperations.moveDirectory(source.directoryPath, destination.directoryPath)
            }
        }
    }

    /**
     * Iterates over all song folders in [bmsPacks], then removes any duplicated BMS files that occur in the same song
     * folder more than once.
     * Returns a number of removed files.
     */
    fun cleanPacks(bmsPacks: Collection<Path>): Int {
        var removedFilesCount = 0
        bmsPacks.forEach { pack ->
            pack.forEachPathInDirectory { song ->
                if (song.isADirectory()) {
                    song.getAllPathsInDirectory { !it.isADirectory() && it.hasBMSFileExtension() }
                        .groupBy { FileReader.fileContentToByteArray(it).sha256().hex }
                        .filter { it.value.size > 1 }
                        .flatMap { it.value.sortedBy { path -> path.fileName.length }.subList(1, it.value.size) }
                        .forEach {
                            FilesystemOperations.delete(it)
                            removedFilesCount++
                        }
                }
            }
        }
        return removedFilesCount
    }
}
