package com.sneknology.bmsorganizer.model

import kotlinx.cinterop.*
import platform.posix.*

@Suppress("RemoveRedundantCallsOfConversionMethods") // needed for linux/windows builds
actual fun Path.isADirectory(): Boolean {
    if (value != "." && value != "..") {
        memScoped {
            val stat = alloc<stat>()
            stat(value, stat.ptr)
            return stat.st_mode.toUInt().and(S_IFDIR.toUInt()) != 0u
        }
    } else {
        return false
    }
}

actual fun Path.exists(): Boolean {
    if (value != "." && value != "..") {
        memScoped {
            val stat = alloc<stat>()
            val code = stat(value, stat.ptr)
            return code != -1
        }
    } else {
        return true
    }
}

actual fun Path.getAllPathsInDirectory(filter: (Path) -> Boolean): Set<Path> {
    val allPaths = mutableSetOf<Path>()
    forEachPathInDirectory { if (filter(it)) allPaths.add(it) }
    return allPaths
}

actual fun Path.forEachPathInDirectory(action: (Path) -> Unit) {
    require(this.isADirectory()) { "$this is not a directory!" }
    val directory = opendir(value) ?: throw IllegalArgumentException("Directory $this does not exist!")

    try {
        do {
            val fileName = readdir(directory)?.pointed?.d_name?.toKString()
            if (fileName != null && fileName != "." && fileName != "..") {
                val innerPath = this.append(fileName)
                action(innerPath)
            }
        } while (fileName != null)
    } finally {
        closedir(directory)
    }
}
