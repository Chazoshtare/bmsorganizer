package com.sneknology.bmsorganizer

import com.sneknology.bmsorganizer.containers.deduplicator
import libui.ktx.appWindow
import libui.ktx.tabpane

fun main(args: Array<String>) =
    appWindow("BMSOrganizer", 800, 600) {
        tabpane {
            deduplicator()
        }
    }
