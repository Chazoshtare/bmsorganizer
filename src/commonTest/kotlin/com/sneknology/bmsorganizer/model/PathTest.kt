package com.sneknology.bmsorganizer.model

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

internal class PathTest {

    @Test
    fun `fileName property contains just file name without path leading to it`() {
        // given
        val path = Path("test/path/to/directory/")

        // when
        val fileName = path.fileName

        // then
        assertEquals("directory", fileName)
    }

    @Test
    fun `fileName property contains just file name if there is no path leading to it`() {
        // given
        val path = Path("file.txt")

        // when
        val fileName = path.fileName

        // then
        assertEquals("file.txt", fileName)
    }

    @Test
    fun `hasBMSFileExtension returns true for all BMS formats`() {
        // given
        val bmsFormats = setOf("bms", "bme", "bml", "pms", "bmson")
        val paths = bmsFormats.map { Path("someFile.$it") }

        // when
        val actual = paths.map { it.hasBMSFileExtension() }

        // then
        actual.forEach { assertTrue(it) }
    }

    @Test
    fun `hasBMSFileExtension returns false for any non-BMS formats`() {
        // given
        val bmsFormats = setOf("bmss", "wav", "ogg", "sd9", "bmsone")
        val paths = bmsFormats.map { Path("someFile.$it") }

        // when
        val actual = paths.map { it.hasBMSFileExtension() }

        // then
        actual.forEach { assertFalse(it) }
    }
}
