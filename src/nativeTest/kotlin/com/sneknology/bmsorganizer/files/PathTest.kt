package com.sneknology.bmsorganizer.files

import com.sneknology.bmsorganizer.model.Path
import com.sneknology.bmsorganizer.model.exists
import com.sneknology.bmsorganizer.model.isADirectory
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

internal class PathTest {

    @Test
    fun `isADirectory returns true for a directory`() {
        // given
        val path = Path("src/nativeTest/resources/directory")

        // when
        val isADirectory = path.isADirectory()

        // then
        assertTrue(isADirectory)
    }


    @Test
    fun `isADirectory returns false for a file`() {
        // given
        val path = Path("src/nativeTest/resources/directory/file.txt")

        // when
        val isADirectory = path.isADirectory()

        // then
        assertFalse(isADirectory)
    }


    @Test
    fun `exists returns true if a path exists`() {
        // given
        val path = Path("src/nativeTest/resources/directory")

        // when
        val exists = path.exists()

        // then
        assertTrue(exists)
    }

    @Test
    fun `exists returns false if a does not exist`() {
        // given
        val path = Path("src/nativeTest/resources/nodirectory")

        // when
        val exists = path.exists()

        // then
        assertFalse(exists)
    }
}
