package com.sneknology.bmsorganizer.files

import com.sneknology.bmsorganizer.model.Path
import kotlin.test.Test
import kotlin.test.assertEquals

internal class FileReaderTest {

    @Test
    fun `fileContentToByteArray correctly reads file content`() {
        // given
        val path = Path("src/nativeTest/resources/directory/file.txt")

        // when
        val content = FileReader.fileContentToByteArray(path).decodeToString()

        // then
        assertEquals("text content\n", content)
    }
}
