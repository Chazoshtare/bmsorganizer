package com.sneknology.bmsorganizer.files

import com.sneknology.bmsorganizer.model.BMSFile
import com.sneknology.bmsorganizer.model.Path
import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertContains
import kotlin.test.assertEquals

internal class DirectoryScannerTest {

    @Test
    fun `getAllDirectoriesPathsInPath returns all directories in a path`() {
        // given
        val path = Path("src/nativeTest/resources/bms")

        // when
        val directories = DirectoryScanner.getAllDirectoriesInPath(path)

        // then
        val expected = listOf(
            path.append("nuflame"),
            path.append("plasticgarden"),
            path.append("praetorium")
        )
        assertEquals(3, directories.size)
        expected.forEach {
            assertContains(directories, it)
        }
    }

    @Test
    @Ignore // TODO: different hash on windows fails this test
    fun `getAllBMSFilesInPaths finds all BMS files in a directory`() {
        // given
        val path = Path("src/nativeTest/resources/bms")

        // when
        val bmsFiles = DirectoryScanner.getAllBMSFilesInPaths(listOf(path))

        // then
        assertEquals(7, bmsFiles.size)
        assertContains(
            bmsFiles,
            BMSFile(
                path.append("praetorium/praetorium.bms"),
                "e211374a0c38ab0a46d851f875216423eb0d833d52677ea5d7f3f1d87f7e16bc",
                path.append("praetorium"),
                4,
                1
            )
        )
    }
}
