package com.sneknology.bmsorganizer.bms

import com.sneknology.bmsorganizer.model.DuplicatedDirectoryInfo
import com.sneknology.bmsorganizer.model.Path
import kotlin.test.Test
import kotlin.test.assertEquals

internal class DuplicatesOperationsTest {

    @Test
    fun `findDuplicates returns only distinct directories in detected duplicates series`() {
        // given
        val path1 = Path("src/nativeTest/resources/bmswithdupes/pack1")
        val path2 = Path("src/nativeTest/resources/bmswithdupes/pack2")

        // when
        val duplicateGroups = DuplicatesOperations.findDuplicates(listOf(path1, path2))

        // then
        val expectedGroups = listOf(
            DuplicatedDirectoryInfo(Path("src/nativeTest/resources/bmswithdupes/pack1/song1"), 0, 0),
            DuplicatedDirectoryInfo(Path("src/nativeTest/resources/bmswithdupes/pack2/song2"), 0, 0)
        )
        assertEquals(1, duplicateGroups.size)
        assertEquals(expectedGroups, duplicateGroups[0])
    }
}
